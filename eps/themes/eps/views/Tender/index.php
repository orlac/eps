<!--
<script type="text/javascript" src="/eps/js/eps/jfeed/jatom.js"></script>
<script type="text/javascript" src="/eps/js/eps/jfeed/jfeed.js"></script>
<script type="text/javascript" src="/eps/js/eps/jfeed/jfeeditem.js"></script>
<script type="text/javascript" src="/eps/js/eps/jfeed/jrss.js"></script>
-->
<script type="text/javascript" src="/eps/js/eps/jquery-treeview/jquery.treeview.js"></script>
<link rel="stylesheet" href="/eps/css/eps/jquery-treeview/jquery.treeview.css" type="text/css" media="screen">
<style>
.feedItemH6{
    font-size: 17px;
}

._wrapper li {
    border-style: solid;
    border-width: 1px;
    border-bottom: 0px;
    margin-top: 1px;
    border-right: 0px;
    border-left: 0px;
    padding: 5px;
    font-size: 14px;
    
}

._wrapper li a{
    font-weight: bold;
    
}
._wrapper li h6 a{
    color: #0080FF;
}

#paramList li a{
    font-weight: bold;
    font-size: 16px;
}
#paramList #categoryList, #regionList, #trendList, #tenderList {
    padding-left: 10px;
}
#paramList #categoryList a, 
#paramList #regionList a,
#paramList #trendList a,
#paramList #tenderList a {
    font-size: 13px;
}

</style>

<div class="wrapper border-2 bot-pad margin-bot">
    <div class="alpha grid_11 suffix_1">
        <!--<div class="wrapper" id="parseContent">-->
        <div class="_wrapper" style="width: 625px;" >
            <ul id="parseContent">
            </ul>
        </div>
    </div>
    <div class="grid_4 omega" style="padding-top: 17px;">
        <ul class="list-1" id="paramList">
            <li>
                <a href="#0">Тендеры</a>
                <ul id="tenderList">
                    <li>
                        <a href="#0">Категория</a>
                        <ul id="categoryList"></ul>
                    </li>
                    <li>
                        <a href="#0">Регион</a>
                        <ul id="regionList"></ul>
                    </li>    
                </ul>
            </li>
            
            <li>
                <a href="#0">Тренды</a>
                <ul id="trendList"></ul>
            </li>
        </ul>
        
    </div>
</div>

<dir style="display:none;" id="itemTempl" >
    <table class="costList">
        <tr>
            <td style="width: 250px;">
                Название тендера
            </td>
            <td>
                Категория
            </td>
            <td style="width: 150px;">
                Регион
            </td>
            <td style="width: 100px;">
                Сумма контракта (т.р.)
            </td>
            <td>
                Срок окончания подачи заявок
            </td>
        </tr>
    </table>
    <!--
    <li>
        <a href="#0" class="headFeedItem" ></a>
        <ul>
            <div style="width: 100%;" class="contentFeedItem"></div>
            <a class="aOrderEps" href="#here">оформить ЭЦП для участия в торгах</a>
        </ul>
    </li>
    -->
    
</dir>
<script type="text/javascript">
if($.cookie("regionId") != null)
{
    var regionId = $.cookie("regionId");    
}else{
    var regionId = <?print($regionId);?>;
}
var tenderCategorys = [];
<?php
while($category = array_shift($tenderCategorys))
{
    ?>
tenderCategorys.push({id: <?print($category->id);?>, name: "<?print($category->name);?>"})     
    <?
} 
?>
var trends = [];
<?php
while($trend = array_shift($trends))
{
    ?>
trends.push({id: <?print($trend->id);?>, name: "<?print($trend->name);?>"})     
    <?
} 
?>
<?php
/**
 * если не отрендерино, то рендерим
*/
if($regions != null)
{
    ?>
    var regions = [];    
    <?
    while($region = array_shift($regions))
    {
        ?>
    regions.push({id: <?print($region->id);?>, name: "<?print($region->name);?>" }) 
        <?
    }     
}
?>
$(document).ready(function()
{
    buildRightMenu(function()
    {
        loadTenderList();
        initMenu('paramList');
        $("#regionList a").click(function(){
            var url = regionUrlRss.replace("@idRegion", this.id);
            loadTenderList(url);
            //alert(this.id);
        })
        $("#categoryList a").click(function(){
            var url = categoryUrlRss.replace("@idCategory", this.id);
            loadTenderList(url);
            //alert(this.id);
        })        
        $("#trendList a").click(function(){
            var url = trendItemUrl.replace("@id", this.id);
            loadTrendItem(url);
            //alert(this.id);
        })
    })
});


var trendItemUrl = "/eps/trend?id=@id";

var regionUrlRss = "http://www.i-tenders.ru/index.php?nregion=@idRegion&format=rss2"
var categoryUrlRss = "http://www.i-tenders.ru/index.php?nsection=@idCategory&format=rss2"
var defaultUrlRss = "http://www.i-tenders.ru/index.php?nregion="+regionId+"&format=rss2"
//var defaultUrlRss = "http://www.i-tenders.ru/index.php?tenderlist=all&format=rss2";
function loadTenderList(url)
{
    $("#parseContent").html("Загрузка");
    if(url == undefined)
    {
        url = defaultUrlRss;
    }
    $.ajax({
            url: 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
            dataType: 'json',
            success: onGetFeed
        });
}

function loadTrendItem(url)
{
    var html = "<iframe src='"+url+"' style='width:100%;min-height: 650px;' />"
    $("#parseContent").html(html);
    return;
    $.ajax({
        url: url,
        dataType: 'html',
        type : 'GET', 
        success: onLoadTrendItem
    });    
    return false;
}
function onLoadTrendItem(data)
{
    $("#parseContent").html(data); 
}


function buildRightMenu(complete)
{
    buildMenu(tenderCategorys, "categoryList");
    buildMenu(regions, "regionList");
    buildMenu(trends, "trendList");
    overChangeBackGround($("#categoryList li"), "#E5E6E7");
    overChangeBackGround($("#regionList li"), "#E5E6E7");
    overChangeBackGround($("#trendList li"), "#E5E6E7");
    complete();
}

function buildMenu(arr, elementId)
{
    for(var i in arr)
    {
        var html = '<li><a href="#0" id="'+ arr[i].id +'" >'+ arr[i].name +'</a></li>'
        $("#"+elementId+"").append(html);
    }
}

/**
"Сфера деятельности: Строительство общее.
				Регион: Агинский Бурятский автономный округ.
				Дата публикации: ..."

"Сфера деятельности: Строительство общее.
				Регион: Агинский Бурятский автономный округ.
				Дата публикации: 25.11.2011.
				Цена контракта: 292.798 тыс. руб.				Дата окончания подачи заявок: 02.12.2011."

 **/
function onGetFeed(data)
{
    if(data.responseData.feed.entries != undefined)
    {
        var entries = data.responseData.feed.entries;
        var html = "";
        $(".itemTemp").remove();
        for (var i in entries)
        {
            //var templ = $("#itemTempl")//.clone();
            var parse = parseContent(entries[i].content);
            var html = "";
            var html = html + "<tr class='itemTemp'>";
            var html = html + "<td>"+ entries[i].title +"</td>";
            var html = html + "<td>"+ parse.category +"</td>";
            var html = html + "<td>"+ parse.region +"</td>";
            var html = html + "<td><a class='aOrderEps' href='#here'>"+ parse.cost +"</a></td>";
            var html = html + "<td>"+ parse.dateEnd +"</td>";
            var html = html + "</tr>";
            /*templ.find(".headFeedItem").html( entries[i].title ) ;
            templ.find(".titleFeedItem").html( entries[i].title ) ;
            templ.find(".titleFeedItem").attr('href', entries[i].link)
            templ.find(".contentFeedItem").html( entries[i].content );
            parseContent(entries[i].content);
            html = html + templ.html();
            */
            //$("#parseContent").append(templ.html());
            $("#itemTempl table").append(html);
        }
        $("#itemTempl table").append(html);
        $("#parseContent").html($("#itemTempl").html()); 
        //$("#parseContent").html(html); 
    }
    $(".aOrderEps").click(orderEps);
    //initMenu('parseContent');
    overChangeBackGround($("._wrapper li"), "#E5E6E7");
}
var reg = {};
reg.list = {};
reg.list.dateEnd = /Дата окончания подачи заявок: (.*)./i;
reg.list.cost = /Цена контракта: (.*) тыс. руб./i;
reg.list.date = /Дата публикации: (.*)./i;
reg.list.region = /Регион: (.*)./i;
reg.list.category = /Сфера деятельности: (.*)./i;


function parseContent(content)
{
    var result = {};
    for(var i in reg.list)
    {
        var temp = reg.list[i].exec(content);
        try{
            if(typeof(temp[1]) != "undefined")
                result[i] = temp[1];
            content = content.replace(temp[0], '');
        }catch(error){
            result[i] = "";
        }
    }
    return result;
}

function overChangeBackGround(jElement, color)
{
    jElement.mouseover(function()
    {
        this.style.cssText = "background-color: "+color+";"
    })
    jElement.mouseout(function()
    {
        this.style.cssText = ""
    })
}

function orderEps()
{
    if(confirm("Желаете оформить ЭЦП для участия в торгах?"))
    {
        window.location.href = "/eps/cost";
    }else{
        window.location.href = "http://www.i-tenders.ru/index.php?tenderlist=all";
    }
}

function initMenu(mainUlId) 
{
    Menu.acordionIni(mainUlId);
}
</script>