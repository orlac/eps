<style>
#aSelectedRegion{
    font-size: 13px;
    line-height: 20px;
}
#divRegionList a{
    font-size: 15px;
}
</style>

<a href="#0" id="aSelectedRegion">
    <strong id="strongSelectedRegion">
        Ваш регион
    </strong>
</a>
<ul style="display: none; visibility: hidden; width: 230px; ">
    <div id="divRegionList" style="width:230px; max-height:550px;overflow-y: scroll;overflow-x:hidden">        
    </div>
</ul>

<script type="text/javascript">
if($.cookie("regionId") != null)
{
    var regionId = $.cookie("regionId");    
}else{
    var regionId = <?print($regionId);?>;
}

<?php
/**
 * если не отрендерино, то рендерим
*/
if($regions != null)
{
    ?>
    var regions = [];  
    <?
    while($region = array_shift($regions))
    {
        ?>    
    regions.push({id: <?print($region->id);?>, name: "<?print($region->name);?>" }) 
        <?
    }     
}
?>

$(document).ready(function()
{
    buildMainMenuRegion(function()
    {
        $(".aSelectRegion").click(SelectRegion)
    });
});

function SelectRegion()
{
    $.cookie("regionId", this.id, {path: '/'});
    var region = getRegionById(this.id);
    $("#strongSelectedRegion").html(region.name);        
}

function buildMainMenuRegion(complete)
{
    var html = "";
    for(var i in regions)
    {
        var item = '<li><a class="aSelectRegion" href="#0" id="'+ regions[i].id +'">'+ regions[i].name +'</a></li>'
        html = html + item;
        if(regions[i].id == regionId)
        {
            $("#strongSelectedRegion").html(regions[i].name);
        } 
    }
    $("#divRegionList").html(html);
    complete();
}

function getRegionById(id)
{
    for(var i in regions)
    {
        if(regions[i].id == id)
        {
            return regions[i];
        }
    }
}
</script>