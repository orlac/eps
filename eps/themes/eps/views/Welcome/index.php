<style>
#messageContent{
    background-color: #FCFBF7;
    padding: 10px;
}
</style>

<div class="alpha grid_11" id="divAlphaGrid_11" style="min-height: 100px;">
    <div class="border-1" id="messageContent" >
        <?php
        print($message);
        ?>
    </div>
</div>
<script type="text/javascript">

function loadMessage()
{
    var options = {};
    options.success = success;
    options.dataType = 'html';
    options.cache = false;
    options.url = '/eps/message/'; 
    $.ajax(options)
}
function success(data)
{
    $("#messageContent").slideUp(300, function()
    {     
        $("#messageContent").html(data);
        $("#messageContent").slideDown(300);
            
    });
    setTimeout(loadMessage, reloadTime);
}
</script>