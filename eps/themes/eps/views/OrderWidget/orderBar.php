<script>
var defFormOrderName = "Ваше имя:";
var defFormOrderMail = "Ваш e-mail:";
var defFormOrderPhone = "Номер Вашего телефона:";
</script>
<style>
#divErrorOrder p{
    font-size: 12px;
    color: red;
    font-weight: bold;
}
</style>
<dir style="display: none;" id="divOrderForm" >
    <div style="width: 200px;height: 130px;">
        <div id="divErrorOrder"></div>
        <form name="quick" id="quick-order" style="width: 200px;height: 150px;" >
            
            <label class="label-1">
                <input class="quicktext" name="OrderForm[name]" id="formOrderName"
                        type="text" 
                        onblur="if(this.value=='') this.value=defFormOrderName" 
                        onfocus="if(this.value ==defFormOrderName ) this.value=''"  />
            </label>
            
            <label class="label-1">
                <input class="quicktext" name="OrderForm[mail]" id="formOrderMail" 
                        type="text" 
                        onblur="if(this.value=='') this.value=defFormOrderMail" 
                        onfocus="if(this.value ==defFormOrderMail ) this.value=''" />
            </label>
            
            <label class="label-1">
                <input class="quicktext" name="OrderForm[phone]" id="formOrderPhone" 
                        type="text"
                        onblur="if(this.value=='') this.value=defFormOrderPhone" 
                        onfocus="if(this.value ==defFormOrderPhone ) this.value=''" />
            </label>
            
            <div class="alignright indent-r">
                <span class="button2">
                    <a href="#0" id="formOrderSubmit">
                        Заказать
                    </a>
                </span>
            </div>
        </form>
    </div>
</dir>


<script type="text/javascript">

$(document).ready(function()
{
    $("#formOrderName").val(defFormOrderName);
    $("#formOrderMail").val(defFormOrderMail);
    $("#formOrderPhone").val(defFormOrderPhone);
    
    $(".create_order").click(showOrderWindow);
    $("#formOrderSubmit").click(sendOrder)    
})

var selectedIdCost = 0;
function showOrderWindow()
{
    selectedIdCost = this.id;
    $("#divOrderForm").modal({
    	containerCss:{
    		height:200,
    		width:330
    	},
    	overlayClose:true
    });
}

function sendOrder()
{
    var mail = ($("#formOrderMail").val() != defFormOrderMail) ? $("#formOrderMail").val() : "";
    var name = ($("#formOrderName").val() != defFormOrderName) ? $("#formOrderName").val() : "";
    var phone = ($("#formOrderPhone").val() != defFormOrderPhone) ? $("#formOrderPhone").val() : "";

    var url = "/eps/order"
    
    $.ajax({
            url: url,
            dataType: 'json',
            type : 'POST',
            data: { 
                OrderForm: 
                {
                    mail: mail,
                    name: name,
                    phone: phone,
                    cost: selectedIdCost,
                    submit: true        
                }
            }, 
            success: onSendOrder
        });
}

function onSendOrder(data)
{
    if(data.success != true)
    {
        var errorText = "";
        for(var i in data.errors)
        {
            for(var j in data.errors[i])
            {
                //errorText += "<p>"+ data.errors[i][j] +"</p>";
                errorText += data.errors[i][j]+"\n";
            }
        }
        alert(errorText);
        //$("#divErrorOrder").html(errorText);
        
    }else
    {
        $("#divErrorOrder").html("");
        alert(data.message);
        $.modal.close();
    }
}
</script>