<script>
var defFormQuestName = "Ваше имя:";
var defFormQuestMail = "Ваш e-mail:";
var defFormQuestQuest = "Задайте вопрос:";
</script>
<div id="divErrorQuest"></div>

<form name="quick" id="quick-form">
    <label class="label-1">
        <input class="quicktext" name="QuestForm[mail]" id="formQuestMail" 
                type="text" 
                onblur="if(this.value=='') this.value=defFormQuestMail" 
                onfocus="if(this.value ==defFormQuestMail ) this.value=''" />
    </label>
    <label class="label-1">
        <input class="quicktext" name="QuestForm[name]" id="formQuestName"
                type="text"  
                onblur="if(this.value=='') this.value=defFormQuestName" 
                onfocus="if(this.value ==defFormQuestName ) this.value=''" />
    </label>
    <label class="label-2">
        <textarea class="quickmessage" id="formQuestQuest"
                onblur="if(this.value=='') this.value=defFormQuestQuest" 
                onfocus="if(this.value ==defFormQuestQuest ) this.value=''"
                name="QuestForm[quest]" ></textarea>
    </label>
    <div class="alignright indent-r">
        <span class="button2">
            <a href="#0" id="formQuestSubmit">
                Отправить
            </a>
        </span>
    </div>
</form>

<script type="text/javascript">

$(document).ready(function()
{
    $("#formQuestName").val(defFormQuestName);
    $("#formQuestMail").val(defFormQuestMail);
    $("#formQuestQuest").val(defFormQuestQuest);
    
    $("#formQuestSubmit").click(sendQuest)    
})

function sendQuest()
{
    var mail = ($("#formQuestMail").val() != defFormQuestMail) ? $("#formQuestMail").val() : "";
    var name = ($("#formQuestName").val() != defFormQuestName) ? $("#formQuestName").val() : "";
    var quest = ($("#formQuestQuest").val() != defFormQuestQuest) ? $("#formQuestQuest").val() : "";

    var url = "/eps/quest"
    
    $.ajax({
            url: url,
            dataType: 'json',
            type : 'POST',
            data: { 
                QuestForm: 
                {
                    mail: mail,
                    name: name,
                    quest: quest,
                    submit: true        
                }
            }, 
            success: onSendQuest
        });
}

function onSendQuest(data)
{
    if(data.success != true)
    {
        var errorText = "";
        for(var i in data.errors)
        {
            for(var j in data.errors[i])
            {
                errorText += "<p>"+ data.errors[i][j] +"</p>";
            }
        }
        $("#divErrorQuest").html(errorText);
        
    }else
    {
        //$("#divErrorQuest").html("");
        //$("#quick-form").html(data.message);
        alert(data.message);
    }
}
</script>