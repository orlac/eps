<?
header('Content-Type:text/html; charset=utf-8');
header('Access-Control-Allow-Origin: *');
?>
<!DOCTYPE html>
<!-- saved from url=(0055)http://livedemo00.template-help.com/wt_35756/index.html -->
<html lang="en" class="cufon-active cufon-ready">
<head>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="/eps/css/eps/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/eps/css/eps/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/eps/css/eps/grid.css" type="text/css" media="screen">   
    <script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script>
    <script src="/eps/js/eps/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="/eps/js/eps/cufon-yui.js" type="text/javascript"></script>
    <style type="text/css">
        cufon{text-indent:0!important;}@media screen,projection{cufon{display:inline!important;display:inline-block!important;position:relative!important;vertical-align:middle!important;font-size:1px!important;line-height:1px!important;}cufon cufontext{display:-moz-inline-box!important;display:inline-block!important;width:0!important;height:0!important;overflow:hidden!important;text-indent:-10000in!important;}cufon canvas{position:relative!important;}}@media print{cufon{padding:0!important;}cufon canvas{display:none!important;}}
    </style>
    <script src="/eps/js/eps/cufon-replace.js" type="text/javascript"></script> 
    <script src="/eps/js/eps/superfish.js" type="text/javascript"></script>
    <script src="/eps/js/eps/FF-cash.js" type="text/javascript"></script> 
    <script src="/eps/js/eps/script.js" type="text/javascript"></script>
    <script src="/eps/js/eps/KleinSlabserif-Light_300.font.js" type="text/javascript"></script>
    <script src="/eps/js/eps/KleinSlabserifBlaxX_500.font.js" type="text/javascript"></script>
	<script src="/eps/js/eps/Lane_-_Narrow_400.font.js" type="text/javascript"></script>
    <script src="/eps/js/eps/Trebuchet_MS_700.font.js" type="text/javascript"></script>
	<script src="/eps/js/eps/tms-0.3.js" type="text/javascript"></script>
    <script src="/eps/js/eps/tms_presets.js" type="text/javascript"></script>
    <script src="/eps/js/eps/jquery.easing.1.3.js" type="text/javascript"></script>
    <script src="/eps/js/eps/easyTooltip.js" type="text/javascript"></script>    
    <script src="/eps/js/eps/selectmenu.js" type="text/javascript"></script>
    <script src="/eps/js/eps/jquery.cookie.js" type="text/javascript"></script>
    <script src="/eps/js/eps/ajax.js" type="text/javascript"></script>
    <script type="text/javascript" src="/eps/js/jquery.scrollTo-min.js"></script>
    <link type='text/css' href='/eps/css/simplemodal/basic.css' rel='stylesheet' media='screen' />
    <!-- IE 6 hacks -->
    <!--[if lt IE 7]>
    <link type='text/css' href='css/basic_ie.css' rel='stylesheet' media='screen' />
    <![endif]-->
    <script type="text/javascript" src="/eps/js/simplemodal/jquery.simplemodal.js"></script>
    <script type="text/javascript">
		$(document).ready(function() { 
			$(".list-services a.tooltips").easyTooltip();			
		}); 
	</script>
	<!--[if lt IE 7]>
        <div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100//eps/images/eps/banners/warning_bar_0000_us.jpg" border="0"  alt="" /></a>
        </div>
	<![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="/eps/js/eps/html5.js"></script>
        <link rel="stylesheet" href="/eps/css/eps/ie.css" type="text/css" media="screen">
	<![endif]-->
    <?php
        $this->widget('SocialWidget', array(
            'InitOnly' => true,
        ));
    ?>
</head>
<body id="page1">

	<!--==============================header=================================-->
    <header id="headPage">
    	<div class="main p2 index-2">
			<div class="container_16">
            	<div class="wrapper">
                    <div class="grid_7 suffix_4" style="width: 50%; padding-right: 0px;">
                        <h1><a href="/"></a><span>Professional Painting Contractor</span></h1>
                    </div>
                    <div class="grid_5" style="width: 40%;">
                        <div class="search-style">
                            <?php
                                $this->widget('SocialWidget', array(
                                    'PageTitle' => $this->pageTitle,
                                    'Buttons' => array(
                                        SocialWidget::FbShare,
                                        SocialWidget::VkShare,
                                        SocialWidget::GPlus)
                                ));
                            ?>
                        </div>
                    </div>
                </div>
                <div class="grid_16">
                    <nav>
                    
                        <ul class="menu sf-js-enabled">
                            <li>
                                <a href="/eps/about/" id="aboutMenu" class="ajaxLink" >
                                    <strong>
                                        Об ЭЦП
                                    </strong>
                                </a>
                            </li>
                            <li>
                                <a href="/eps/get/" id="getMenu" class="ajaxLink" >
                                    <strong>
                                        Как получить
                                    </strong>
                                </a>
                            </li>
                            <li>
                                <a href="/eps/cost/" id="costMenu" class="ajaxLink">
                                    <strong>
                                        Сколько стоит
                                    </strong>
                                </a>
                            </li>
                            <li>
                                <?php
                                $this->widget('Tender', array(
                                    "place" => Tender::MENU
                                ));
                                ?>
                            </li>
                            
                            <!--
                            <li><a href="/eps/gii"><strong><cufon class="cufon cufon-canvas" alt="gii" style="width: 104px; height: 18px; "><canvas width="116" height="17" style="width: 116px; height: 17px; top: 1px; left: -1px; "></canvas><cufontext>gii</cufontext></cufon></strong></a></li>
                            -->
                            
                        </ul>
                    </nav>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    	<div class="wrapper index-1">    
            <div class="slider-wrapper">
                <div class="slider-padding">
                <div id="mask">
                    <div></div>
                </div>
                <?php print_r($content) ;?>            
                </div>
            </div>
		</div>
    </header>
    
	<!--==============================content================================-->
    <section id="content">
        <div class="main">
            <div class="container_16">
            	<div class="grid_16">
                    <div class="wrapper border-2 p3">
                        <?php
                        $this->widget('Welcome');
                        $this->widget('Sponsor');
                        ?>
                        
                    </div>
                    <?php
                        $this->widget('Tender', array(
                            "place" => Tender::DOWN
                        ));
                    ?>
                    <div class="wrapper">
                        <div class="alpha grid_6">
                            <div class="indent-top2">
                                <div class="wrapper">
                                    <figure class="fleft"><img src="/eps/images/eps/ico1.png" alt=""></figure>
                                    <?php
                                    $this->widget('Info', array('Page' => Info::FIND));
                                    ?>
                                </div>
                                <div class="wrapper">
                                    <figure class="fleft"><img src="/eps/images/eps/ico2.png" alt=""></figure>
                                    <?php
                                    $this->widget('Info', array('Page' => Info::STATISTIC));
                                    ?>
                                </div>
                                <div class="wrapper">
                                    <figure class="fleft"><img src="/eps/images/eps/ico3.png" alt=""></figure>
                                    <?php
                                    $this->widget('Info', array('Page' => Info::PRICE));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="grid_3 suffix_1">
                            <?php
                            $this->widget('Info', array('Page' => Info::LINKS));
                            ?>
                        </div>
                        <div class="grid_6 omega">
                            <?php
                            $this->widget('Quest', array('Place' => Quest::FORM));
                            ?>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>
    
	<!--==============================footer=================================-->
    <footer>
        <div class="main">
        	<div class="container_16">
            	<div class="grid_16">
                    <div class="wrapper footer-border">
                        <div class="alpha grid_5 suffix_7" style="width: 640px;padding-right: 10px;">
                           <?php
                            $this->widget('BottomText');
                            ?> 
                        </div>
                        <div class="grid_4 omega">
                            <?php
                                $this->widget('SocialWidget', array(
                                    'PageTitle' => $this->pageTitle,
                                    'Buttons' => array(
                                        SocialWidget::FbShare,
                                        SocialWidget::VkShare,
                                        SocialWidget::GPlus)
                                ));
                            ?>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </footer>
	<script type="text/javascript"> Cufon.now(); </script>
    
	<script type="text/javascript">
	$(window).load(function(){
		$('.slider')._TMS({
			duration:800,
			easing:'easeOutQuad',
			preset:'simpleFade',
			pagination:'.pagination',
			slideshow:7000,
			banners:'fade',
			waitBannerAnimation:false,
			pauseOnHover:true
		})
	});
	</script>
	<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7078796-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


</body></html>