var Loader = {};
Loader.tempStyle = "";
Loader.url = "";
Loader.wait = false;
Loader.activeLink = "";

Loader.addEvent = function()
{ 
    $('.ajaxLink').unbind('click mouseleave');    
    $(".ajaxLink").click(Loader.loadPage);
}

Loader.loadPage = function()
{
    document.close();
    Loader.url = this.href;
    if(Loader.url == Loader.activeLink /*|| Loader.url == document.location.href*/)
    {
        return false
    }
    if(Loader.wait == true)
    {
        return false
    }
    Loader.wait = true;
    
    Loader.preLoad(function()
    {
        $.ajax({
            url: Loader.url,
            dataType: 'html',
            type : 'GET',
            data: {ajax: true}, 
            success: Loader.onLoadPage
        });    
    });
    Loader.activeLink = Loader.url;
    try
    {
        SocP.link = Loader.activeLink;
    }catch(e)
    {
        
    }
    return false;
}

Loader.onLoadPage = function(data)
{
    $("#content").html(data);
    Loader.wait = false;    
    Menu.ini();
    $.scrollTo('#headPage', {duration:300});
    Loader.preLoader.hide();
    Loader.addEvent();
    if(pageName != undefined)
    {
        document.title = pageName;
    }
}

Loader.preLoad = function(onHide)
{
    Loader.preLoader.show();
    onHide();
}

Loader.preLoader = {};
Loader.preLoader.show = function()
{
    $('#mask').attr("style", "display:block;");
}
Loader.preLoader.hide = function()
{
    $('#mask').attr("style", "display:none;");
}


$(document).ready(function()
{
    Loader.addEvent();
    //$("#preloader").slideUp(300);
    Loader.preLoader.hide();
});