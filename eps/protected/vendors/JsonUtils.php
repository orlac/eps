<?php

class JsonUtils
{
    public static function renderJson($data)
    {
        header('Content-Type:text/html; charset=utf-8');
        print_r( json_encode( $data ) );
        Yii::app()->end();
    }
}

?>