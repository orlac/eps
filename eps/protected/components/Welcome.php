<?php

Class Welcome extends CWidget
{
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $model = Message::model();
        $message = $model->getOneRandom();
        $this->render('index', array(
            'message' => $message->text
        ) );
    }
}
?>