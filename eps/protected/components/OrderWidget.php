<?php

Class OrderWidget extends CWidget
{
    
    const PAGE = 0;
    const TABLE = 1;
    
    private $place;
    private $idCost;
    
    private $templates = array(
        self::PAGE => 'buttonBar',
        self::TABLE => 'orderBar',
    );
    
    public function setPlace($place)
    {
        $this->place = $place;    
    }
    
    public function setIdCost($idCost)
    {
        $this->idCost = $idCost;    
    }
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {   
        $this->render($this->templates[$this->place], array(
            'idCost' => $page->idCost,
        ) );        
    }
}