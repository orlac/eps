<?php
abstract class CheckRegion extends CWidget
{
    protected $regionId,
              $region, $city  ;
    
    
    public function __construct()
    {
        
        $this->geo_info($this->getRealIpAddr());
        
        //$this->geo_info("91.219.5.52");
        //print_r(iconv("cp1251", "utf-8", $this->region) );
        $model = Region::model();
        if( $this->region != "" )
        {
            $region = $model->getIdByName($this->region);
            $this->regionId = $region->id;   
        }else
        {
            $this->regionId = 506;        
        }
        parent::__construct();
    }
    
    /**    
    */
    protected function geo_info($ip)
    {
        $xml = '<ipquery><fields><city/><region/></fields><ip-list>'
                . '<ip>'.$ip.'</ip></ip-list></ipquery>';
        $ch = curl_init('http://194.85.91.253:8090/geo/geo.html');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: text/xml; charset=utf-8"
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $result = curl_exec($ch);
        if(curl_errno($ch) != 0)
        die('curl_errno('.curl_errno($ch).'), curl_error('.curl_error($ch).')');
        curl_close($ch);
        if (strpos($result, '<message>Not found</message>') !== false)
        return false;
        //print_r($result);
        preg_match('/<city>(.*)<\/city>/', $result, $city);
        preg_match('/<region>(.*)<\/region>/', $result, $region);
        $this->region = (iconv("cp1251", "utf-8", $region[1]) );
    }
    
    protected function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}

?>