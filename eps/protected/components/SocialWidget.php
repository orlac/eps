<?php

Class SocialWidget extends CWidget
{
    protected $title;
    
    const FbShare = 0,
          VkShare = 1,
          FbLike = 2,
          VkLike = 3,
          GPlus = 4;
    
    private $buttons = array(),
            $initOnly = false;
    
    
    public function setPageTitle($title)
    {
        $this->title = $title;    
    }
    
    public function setInitOnly($bool)
    {
        $this->initOnly = $bool;    
    }
    
    public function setButtons($arr)
    {
        
        $this->buttons = $arr;
    }
    
    public function run()
    {   
        $this->render("index", array(
                'pageTitle' => $this->title,
                'buttons' => $this->buttons,
                'idVk' => rand(55555, 99999999),
                'initOnly' => $this->initOnly));        
    }
}