<?php

Class Sponsor extends CWidget
{
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $model = StaticPage::model();
        $message = $model->getPage('sponsor');
        $this->render('index', array(
            'message' => $message->content
        ) );
    }
}
?>