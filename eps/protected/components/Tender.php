<?php
//Yii::import("eps.protected.models.Region");
class Tender extends CheckRegion
{
    
    const MENU = "menu";
    const DOWN = "down";
    
    private $place;
    private $regions;
    private $tenderCategorys;
    private $trends;
    
    public static $renderedRegions = false;
    
    /**
     * ��� ���������� - � ���� ��� � ����
    */
    public function setPlace($place)
    {
        $this->place = $place;
    }
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $regionModel = Region::model();
        $this->regions = $regionModel->getAll();
        
        $tenderCategoryModel = TenderCategory::model();
        $this->tenderCategorys = $tenderCategoryModel->getAll();
        
        $trendsModel = TrendModel::model();
        $this->trends = $trendsModel->getAll();
        switch($this->place)
        {
            case self::MENU:
                $this->rumMenu();
            break;
            case self::DOWN:
                $this->rumDown();
            break;        
        }
    }
    
    private function rumMenu(){
        
        $this->render('menu', array(
            "regionId" => $this->regionId,
            "regions" => (self::$renderedRegions != true) ? $this->regions : null
        ));
        self::$renderedRegions = (self::$renderedRegions != true) ? true : false ;
    }
    private function rumDown(){
        $this->render('index', array(
            "regionId" => $this->regionId,
            "regions" => (self::$renderedRegions != true) ? $this->regions : null,
            "tenderCategorys" => $this->tenderCategorys,
            "trends" => $this->trends
        ));
        self::$renderedRegions = (self::$renderedRegions != true) ? true : false ;
    }
}

?>