<?php

Class Quest extends CWidget
{
    const PAGE = 0;
    const FORM = 1;
    
    private $place;
    
    private $templates = array(
        self::PAGE => 'buttonBar',
        self::FORM => 'form',
    );
    
    public function setPlace($place)
    {
        $this->place = $place;    
    }    
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {   
        $params = array();
        if($this->place == self::FORM)
        {
            $params['form'] = new QuestForm();
        }
        $this->render( $this->templates[$this->place], $params );        
    }
}