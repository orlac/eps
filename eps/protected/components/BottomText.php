<?php

Class BottomText extends CWidget
{
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $model = StaticPage::model();
        $message = $model->getPage('bottomText');
        $this->render('index', array(
            'message' => $message->content
        ) );
    }
}
?>