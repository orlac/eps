<?php
class Tender extends CheckRegion
{
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $regionModel = Region::model();
        $regions = $regionModel->getAll();
        
        $this->render('index', array(
            "regionId" => $this->regionId,
            "regions" => $regions
        ));
    }
}

?>