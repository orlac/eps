<?php

Class Info extends CWidget
{
    
    const FIND = 'find';
    const STATISTIC = 'statistic';
    const LINKS = 'links'; 
    const PRICE = 'adsprice';
    
    private $page;
    private $templates = array(
        self::FIND => 'map',
        self::STATISTIC => 'index',
        self::PRICE => 'index',
        self::LINKS => 'links',
    );
    
    public function setPage($page)
    {
        $this->page = $page;
    }
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {   
        $model = StaticPage::model();
        $page = $model->getPage($this->page);
        
        $this->render($this->templates[$this->page], array(
            'title' => $page->title,
            'page' => $page->page,
            'content' => $page->content,
        ) );
    }
}
?>