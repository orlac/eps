<?php

class CostController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Lists all models.
	 */
	public function actionIndex($ajax = false)
	{
		$dataProvider=new CActiveDataProvider('Cost'/*, array(
        'pagination'=>array(
            'pageSize'=>2,
        )) */);
        //$test = $dataProvider->getData();
        //print_r($test);
        $this->pageTitle = "Лучшие цены";
        $renderData = array(
			'dataProvider'=>$dataProvider,
		    'pageName'=>'cost'  );
        
        if(!$ajax)
            $this->render('index', $renderData);
        else
            $this->renderAjax('index', $renderData);
            
		/*$this->render('index',array(
			'dataProvider'=>$dataProvider,
		    'pageName'=>'cost'  )
        );*/
	}

}
