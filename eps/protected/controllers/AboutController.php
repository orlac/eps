<?php

class AboutController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
    
    protected $page = "about";
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($ajax = false)
	{ 
        $model = StaticPage::model();
        //$page = $model->getPage($this->page);
        $page = $model->getPages($this->page);
        $this->pageTitle = $page->title; 
        $renderData = array(
            'page' => $page,
            'pageName'=>$this->page
        );
        if(!$ajax)
            $this->render('index', $renderData);
        else
            $this->renderAjax('index', $renderData);
	}


}
