<?php

class MessageController extends Controller
{
	public function actionIndex()
	{
		$model = Message::model();
        $message = $model->getOneRandom();
        $this->renderPartial('index', array(
            'message' => $message->text
        ) );
	}
}