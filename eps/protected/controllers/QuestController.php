<?php
Yii::import('application.vendors.*');
class QuestController extends Controller
{
    /**
	 * Lists all models.
	 */
	public function actionIndex()
	{
    	
        $model=new QuestForm();
        if(isset($_POST['QuestForm']))
        {
            $result = array();
            // получаем данные от пользователя
            $model->attributes=$_POST['QuestForm'];
            // проверяем полученные данные и, если результат проверки положительный,
            // перенаправляем пользователя на предыдущую страницу
            if($model->validate())
            {
                $headers="From: {".$model->mail."}\r\nReply-To: {".$model->mail."}";
				if( mail(Yii::app()->params['adminEmail'], "Вопрос от ".$model->name, $model->quest, $headers) )
                {
                    $result['success'] = true;    
                    $result['message'] = "отправлено...";
                }else
                {
                    $result['success'] = false;
                    $result['errors'] = array();
                    $result['errors'][] = "Какая-то ошибка, письмо не отправлено ;)";
                }
                
            }else{
                $result['success'] = false;
                $result['errors'] = $model->getErrors();   
            }
            JsonUtils::renderJson($result);
        }
     } 
    	
}
