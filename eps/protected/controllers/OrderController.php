<?php
Yii::import('application.vendors.*');
class OrderController extends Controller
{
    /**
	 * Lists all models.
	 */
	public function actionIndex()
	{
    	
        $model=new OrderForm();
        if(isset($_POST['OrderForm']))
        {
            $result = array();
            // получаем данные от пользователя
            $model->attributes=$_POST['OrderForm'];
            // проверяем полученные данные и, если результат проверки положительный,
            // перенаправляем пользователя на предыдущую страницу
            if($model->validate())
            {
                $order = new Order();
                
                $order->name = $model->name;
                $order->phone = $model->phone;
                $order->mail = $model->mail;
                $order->eps_cost = $model->cost;
                
                if(!$order->save())
                {
                    $result['success'] = false;
                    $result['errors'] = array();
                    $result['errors'][] = "Какая-то ошибка, заказ не отправлен ;)";
                    JsonUtils::renderJson($result);
                }else
                {
                    $headers="From: {".$model->mail."}\r\nReply-To: {".$model->mail."}";
                    
                    $body = $model->name."\n";
                    $body .= $model->phone."\n";
                    $body .= $model->mail."\n";
                    mail(Yii::app()->params['adminEmail'], "Заказ от ".$model->name, $body, $headers);
                    $result['success'] = true;    
                    $result['message'] = "отправлено...";
                    JsonUtils::renderJson($result);
                }
                
            }else{
                $result['success'] = false;
                $result['errors'] = $model->getErrors();   
            }
            JsonUtils::renderJson($result);
        }
     }
    	
}
