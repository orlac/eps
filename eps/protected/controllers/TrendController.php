<?php

class TrendController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
    
    protected $page = "about";
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id = 2)
	{ 
        $model = TrendModel::model();
        
        $criteria=new CDbCriteria;
		$criteria->compare('id', intval($id) );
        $page = $model->find($criteria); 
        $renderData = array(
            'page' => $page->html,
        );
        //print($page->html);
        $this->renderAjax('index', $renderData);
	}


}
