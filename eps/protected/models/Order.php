<?php

/**
 * This is the model class for table "eps_orders".
 *
 * The followings are the available columns in table 'eps_orders':
 * @property integer $id
 * @property integer $eps_cost
 * @property string $name
 * @property string $mail
 */
class Order extends CActiveRecord
{
	const EPS_COST = 'eps_cost';
    
    //public $id, $name, $mail, $phone, $eps_cost, $good; 
    /**
	 * Returns the static model of the specified AR class.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'eps_orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('eps_cost', 'numerical', 'integerOnly'=>true),
			array('name, mail', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, eps_cost, name, mail', 'safe', 'on'=>'search'),
		);
	}
       

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'good' => array(self::BELONGS_TO, 'Cost', 'eps_cost' )
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'eps_cost' => 'id ЭПЦ',
            'good.type' => 'ЭЦП',
			'name' => 'Имя',
			'mail' => 'e-mail',
            'phone' => 'телефон',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('eps_cost',$this->eps_cost);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('mail',$this->mail,true);
        //$criteria->compare('good',$this->good,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}