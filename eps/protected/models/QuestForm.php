<?php

class QuestForm extends CFormModel
{
    public $mail, $name, $quest;
    
    public function rules()
	{
		return array(
			// username and password are required
			array('mail, name, quest', 'required',
                  'message'=>'Введите значение {attribute}.'  ),
            array('mail', 'email',
                  'message'=>'{attribute} не соответствует формату .' ),
            
		);
	}
    
    /**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'mail'=>'Ваш e-mail',
            'name'=>'Ваше Имя',
            'quest'=>'Вопрос',
		);
	}
}