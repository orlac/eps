<?php

/**
 * This is the model class for table "eps_static".
 *
 * The followings are the available columns in table 'eps_static':
 * @property integer $id
 * @property string $page
 * @property string $content
 */
class StaticPage extends CActiveRecord
{
	
    //public $id, $title, $page, $content, $socialButtons;
    
    /**
	 * Returns the static model of the specified AR class.
	 * @return StaticPage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'eps_static';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, page, content, socialButtons', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, page, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'title' => 'Заголовок',
			'page' => 'Страница',
			'content' => 'html',
                        'socialButtons' => 'кнопки сетей'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
		$criteria->compare('page',$this->page,true);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    public function getPage($page)
    {
        return self::model()->find('page=:page', array('page' => $page));
    }
    public function getPages($page)
    {
        return self::model()->findAll('page=:page', array('page' => $page));
    }
}