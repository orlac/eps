<?php

class OrderForm extends CFormModel
{
    public $mail, $name, $phone, $cost;
    
    public function rules()
	{
		return array(
			// username and password are required
			array('mail, name, phone', 'required',
                  'message'=>'Введите значение {attribute}.'  ),
            array('mail', 'email',
                  'message'=>'{attribute} не соответствует формату .' ),
            array('cost', 'isCost' ),
            
		);
	}
    
    public function isCost($attribute, $params)
    {
        $model = Cost::model();
        if( $model->findByPk($this->cost) != null )
        {
            return true;
        }
        $this->addError('cost','Нет такого типа ЭПЦ');
        return false;
    }
    
    /**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'mail'=>'Ваш e-mail',
            'name'=>'Ваше Имя',
            'phone'=>'телефон',
		);
	}
}