<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(
    // ����������� �� main.php
    require(dirname(__FILE__).'/../../../eps/protected/config/main.php'),
    array(
        'theme'=>"",
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        // autoloading model and component classes
    	'import'=>array(
    		'application.models.*',
    		'application.components.*',
    	),
    )
);
