<link rel="stylesheet" href="/adminko/css/common/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" charset="utf-8"/>
<link rel="stylesheet" href="/adminko/js/common/elrte/css/elrte.css" type="text/css" media="screen" charset="utf-8"/>
 

<script src="/adminko/js/common/jquery-ui-1.8.13.custom.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/adminko/js/common/elrte/js/elRTE.js" type="text/javascript" charset="utf-8"></script>
<script src="/adminko/js/common/elrte/js/i18n/elrte.ru.js" type="text/javascript" charset="utf-8"></script>

<script src="/adminko/js/common/elrte/js/elRTE.DOM.js" type="text/javascript" charset="utf-8"></script>
<script src="/adminko/js/common/elrte/js/elRTE.filter.js" type="text/javascript" charset="utf-8"></script>
<script src="/adminko/js/common/elrte/js/elRTE.history.js" type="text/javascript" charset="utf-8"></script>
<script src="/adminko/js/common/elrte/js/elRTE.options.js" type="text/javascript" charset="utf-8"></script>
<script src="/adminko/js/common/elrte/js/elRTE.selection.js" type="text/javascript" charset="utf-8"></script>
<script src="/adminko/js/common/elrte/js/elRTE.ui.js" type="text/javascript" charset="utf-8"></script>
<script src="/adminko/js/common/elrte/js/elRTE.utils.js" type="text/javascript" charset="utf-8"></script>
<script src="/adminko/js/common/elrte/js/elRTE.w3cRange.js" type="text/javascript" charset="utf-8"></script>



<script src="/adminko/js/common/ellib/js/eli18n.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">

var Elrte = {};

Elrte.options = {
     //lang         : 'ru',   // set your language
     styleWithCSS : false,
     height       : 400,
     toolbar      : 'maxi'
}; 

Elrte.setEditor = function( jQueryElement, options )
{
    var opts = ( options != undefined ) ? options : Elrte.options;
    jQueryElement.elrte(opts);
}
</script>