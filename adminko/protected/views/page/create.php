<?php
$this->breadcrumbs=array(
	'Admin Static Pages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdminStaticPage', 'url'=>array('index')),
	array('label'=>'Manage AdminStaticPage', 'url'=>array('admin')),
);
?>

<h1>Create AdminStaticPage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>