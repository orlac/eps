<?php
$this->breadcrumbs=array(
	'Admin Static Pages',
);

$this->menu=array(
	array('label'=>'Create AdminStaticPage', 'url'=>array('create')),
	array('label'=>'Manage AdminStaticPage', 'url'=>array('admin')),
);
?>

<h1>Admin Static Pages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
