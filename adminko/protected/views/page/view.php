<?php
$this->breadcrumbs=array(
	'Admin Static Pages'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List AdminStaticPage', 'url'=>array('index')),
	array('label'=>'Create AdminStaticPage', 'url'=>array('create')),
	array('label'=>'Update AdminStaticPage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdminStaticPage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdminStaticPage', 'url'=>array('admin')),
);
?>

<h1>View AdminStaticPage #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'page',
		'content',
	),
)); ?>
