<?php
$this->breadcrumbs=array(
	'Admin Regions',
);

$this->menu=array(
	array('label'=>'Create AdminRegion', 'url'=>array('create')),
	array('label'=>'Manage AdminRegion', 'url'=>array('admin')),
);
?>

<h1>Admin Regions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
