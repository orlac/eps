<?php
$this->breadcrumbs=array(
	'Admin Regions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdminRegion', 'url'=>array('index')),
	array('label'=>'Manage AdminRegion', 'url'=>array('admin')),
);
?>

<h1>Create AdminRegion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>