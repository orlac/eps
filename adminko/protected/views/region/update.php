<?php
$this->breadcrumbs=array(
	'Admin Regions'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdminRegion', 'url'=>array('index')),
	array('label'=>'Create AdminRegion', 'url'=>array('create')),
	array('label'=>'View AdminRegion', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdminRegion', 'url'=>array('admin')),
);
?>

<h1>Update AdminRegion <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>