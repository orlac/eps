<?php
$this->breadcrumbs=array(
	'Admin Regions'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List AdminRegion', 'url'=>array('index')),
	array('label'=>'Create AdminRegion', 'url'=>array('create')),
	array('label'=>'Update AdminRegion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdminRegion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdminRegion', 'url'=>array('admin')),
);
?>

<h1>View AdminRegion #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
