<?php
$this->breadcrumbs=array(
	'Admin Static Pages'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdminStaticPage', 'url'=>array('index')),
	array('label'=>'Create AdminStaticPage', 'url'=>array('create')),
	array('label'=>'View AdminStaticPage', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdminStaticPage', 'url'=>array('admin')),
);
?>

<h1>Update AdminStaticPage <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>