<?php
$this->breadcrumbs=array(
	'Admin Trend Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdminTrendModel', 'url'=>array('index')),
	array('label'=>'Manage AdminTrendModel', 'url'=>array('admin')),
);
?>

<h1>Create AdminTrendModel</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php //$this->widget("Elrte"); ?>

<script type="text/javascript">
$(document).ready(function()
{
    if(typeof Elrte != "undefined")
    {
        Elrte.setEditor($("#AdminTrendModel_html"));
    }
})
</script>