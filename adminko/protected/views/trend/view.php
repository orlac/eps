<?php
$this->breadcrumbs=array(
	'Admin Trend Models'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List AdminTrendModel', 'url'=>array('index')),
	array('label'=>'Create AdminTrendModel', 'url'=>array('create')),
	array('label'=>'Update AdminTrendModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdminTrendModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdminTrendModel', 'url'=>array('admin')),
);
?>

<h1>View AdminTrendModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
</hr>
<?php echo $model->html; ?>