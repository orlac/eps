<?php
$this->breadcrumbs=array(
	'Admin Trend Models',
);

$this->menu=array(
	array('label'=>'Create AdminTrendModel', 'url'=>array('create')),
	array('label'=>'Manage AdminTrendModel', 'url'=>array('admin')),
);
?>

<h1>Admin Trend Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
