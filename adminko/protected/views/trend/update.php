<?php
$this->breadcrumbs=array(
	'Admin Trend Models'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdminTrendModel', 'url'=>array('index')),
	array('label'=>'Create AdminTrendModel', 'url'=>array('create')),
	array('label'=>'View AdminTrendModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdminTrendModel', 'url'=>array('admin')),
);
?>

<h1>Update AdminTrendModel <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>