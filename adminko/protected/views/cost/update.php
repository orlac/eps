<?php
$this->breadcrumbs=array(
	'Admin Costs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdminCost', 'url'=>array('index')),
	array('label'=>'Create AdminCost', 'url'=>array('create')),
	array('label'=>'View AdminCost', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdminCost', 'url'=>array('admin')),
);
?>

<h1>Update AdminCost <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>