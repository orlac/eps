<?php
$this->breadcrumbs=array(
	'Admin Costs',
);

$this->menu=array(
	array('label'=>'Create AdminCost', 'url'=>array('create')),
	array('label'=>'Manage AdminCost', 'url'=>array('admin')),
);
?>

<h1>Admin Costs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
