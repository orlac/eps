<?php
$this->breadcrumbs=array(
	'Admin Costs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AdminCost', 'url'=>array('index')),
	array('label'=>'Create AdminCost', 'url'=>array('create')),
	array('label'=>'Update AdminCost', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdminCost', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdminCost', 'url'=>array('admin')),
);
?>

<h1>View AdminCost #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'description',
		'cost',
		'date',
	),
)); ?>
