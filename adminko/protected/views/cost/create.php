<?php
$this->breadcrumbs=array(
	'Admin Costs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdminCost', 'url'=>array('index')),
	array('label'=>'Manage AdminCost', 'url'=>array('admin')),
);
?>

<h1>Create AdminCost</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>