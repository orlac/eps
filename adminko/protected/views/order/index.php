<?php
$this->breadcrumbs=array(
	'Admin Orders',
);

$this->menu=array(
	array('label'=>'Create AdminOrder', 'url'=>array('create')),
	array('label'=>'Manage AdminOrder', 'url'=>array('admin')),
);
?>

<h1>Admin Orders</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
