<?php
$this->breadcrumbs=array(
	'Admin Orders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdminOrder', 'url'=>array('index')),
	array('label'=>'Manage AdminOrder', 'url'=>array('admin')),
);
?>

<h1>Create AdminOrder</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>