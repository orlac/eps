<?php
$this->breadcrumbs=array(
	'Admin Orders'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdminOrder', 'url'=>array('index')),
	array('label'=>'Create AdminOrder', 'url'=>array('create')),
	array('label'=>'View AdminOrder', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdminOrder', 'url'=>array('admin')),
);
?>

<h1>Update AdminOrder <?php echo $model->id;
?></h1>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>