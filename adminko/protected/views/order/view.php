<?php
$this->breadcrumbs=array(
	'Admin Orders'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List AdminOrder', 'url'=>array('index')),
	array('label'=>'Create AdminOrder', 'url'=>array('create')),
	array('label'=>'Update AdminOrder', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdminOrder', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdminOrder', 'url'=>array('admin')),
);
?>

<h1>View AdminOrder #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'eps_cost',
		'name',
		'mail',
	),
)); ?>
