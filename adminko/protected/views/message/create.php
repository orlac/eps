<?php
$this->breadcrumbs=array(
	'Admin Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdminMessage', 'url'=>array('index')),
	array('label'=>'Manage AdminMessage', 'url'=>array('admin')),
);
?>

<h1>Create AdminMessage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>