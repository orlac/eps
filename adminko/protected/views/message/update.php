<?php
$this->breadcrumbs=array(
	'Admin Messages'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdminMessage', 'url'=>array('index')),
	array('label'=>'Create AdminMessage', 'url'=>array('create')),
	array('label'=>'View AdminMessage', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdminMessage', 'url'=>array('admin')),
);
?>

<h1>Update AdminMessage <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>