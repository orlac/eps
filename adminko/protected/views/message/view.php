<?php
$this->breadcrumbs=array(
	'Admin Messages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AdminMessage', 'url'=>array('index')),
	array('label'=>'Create AdminMessage', 'url'=>array('create')),
	array('label'=>'Update AdminMessage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdminMessage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdminMessage', 'url'=>array('admin')),
);
?>

<h1>View AdminMessage #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
	),
)); ?>
</hr>
<?php echo $model->text; ?>