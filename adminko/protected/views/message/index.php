<?php
$this->breadcrumbs=array(
	'Admin Messages',
);

$this->menu=array(
	array('label'=>'Create AdminMessage', 'url'=>array('create')),
	array('label'=>'Manage AdminMessage', 'url'=>array('admin')),
);
?>

<h1>Admin Messages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
